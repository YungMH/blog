class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy #assoicated to comment and delete the post also delete the all comments belongs to this post
	validates_presence_of :title
	validates_presence_of :body
end
